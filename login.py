#!/usr/bin/python
# -*- coding: utf-8 -*-

# login.py
# Copyright (C) 2013 Felipe Garay
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Repositorio: https://bitbucket.org/haskmell/login.py
#
#
# Si encuentras un error reportalo en espanol en:
# https://bitbucket.org/haskmell/login.py/issues?status=new&status=open
#


# Tu usuario y clave para el wifi (deberia ser la misma que en el mail usach)
USUARIO = ""
PASSWORD = ""









import urllib
import urllib2

import commands
import re



URL = "https://ccasw.usach.cl/auth/perfigo_cm_validate.jsp"


# Expresion regular de una ip en el comando ifconfig
IP_RE = re.compile("inet addr:\d+\.\d+\.\d+.\d+")

# Expresion regular de la ip en español
IP_RE_ES = re.compile("Direc. inet:\d+\.\d+\.\d+.\d+")

def concat(l):
    """
    Transforma una lista de listas en una lista solamente
    
            [[a]] -> [a]
    
    Se comporta igual que la funcion concat de Haskell
    
    """
    nl = []
    for x in l:
            for y in x:
                    nl.append(y)
    return nl


def get_i_wlan0():
    """
    Devuelve la ultima ip del comando ifconfig en linux, que asumimos es la
    de wlan0
    """
    ips = map(lambda x: filter(lambda y: y!="inet addr", x.split(":")) , IP_RE.findall(commands.getoutput("/sbin/ifconfig"))) 

    if len(ips) == 0:
	ips = map(lambda x: filter(lambda y: y!="Direc. inet", x.split(":")) , IP_RE_ES.findall(commands.getoutput("/sbin/ifconfig"))) 
    
    ips = filter(lambda x: x != "127.0.0.1", concat(ips))

    return ips[-1] # ultima ip
            
    


def enviar_datos(URL, data):
    """
    Hace una peticion POST a la URL dada con los datos dados en forma de
    diccionario
    """
    data = urllib.urlencode(data)
    req = urllib2.Request(URL, data)
    response = urllib2.urlopen(req)
    lectura = response.read()
    
    return lectura
    


def generar_data(usr, password, ip):
    """
    Genera los datos necesarios para poder loguearnos en el servidor
    """
    data = {
        'cm': "ws32vklm",
        'compact': "false",
        'guestpasswordlabel': "Password",
        'guestusernamelabel': "Guest Id",
        'index': "3",
        'pageid': "-1",
        'password': password,
        'passwordlabel': "Password",
        'pm': "Linux x86_64",
        'provider': "USACH",
        'registerguest': "NO",
        'reqfrom': "perfigo_login.jsp",
        'session': "",
        'uri': "www.google.cl",
        'usernamelabel': "Username",
        'userip': ip,
        'username': usr
    }

    return data








if __name__ == "__main__":
    # ip del notebook
    ip = get_i_wlan0()

    # los datos que vamos a enviar al servidor
    data = generar_data(USUARIO, PASSWORD, ip)
    data_remove_old = data
    data_remove_old['remove_old'] = "1"

    respuesta = enviar_datos(URL, data)
    
    if respuesta.find("You have been successfully logged on the network.") == -1:
        print "Removiendo al usuario viejo"
        
        respuesta = enviar_datos(URL, data_remove_old)
        
        if respuesta.find("You have been successfully logged on the network.") == -1:
                print "Error :("
        
        else:
                print "Entramos"
    else:
        print "Entramos"
